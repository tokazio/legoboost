package fr.tokazio.legoboost;

import okhttp3.*;

import java.io.IOException;

public class Controller {

    public static final MediaType RAW
            = MediaType.get("text/plain; charset=utf-8");
    public static final String URL = "http://localhost:9080/in";

    private final OkHttpClient client = new OkHttpClient();

    public String send(String cmd) throws IOException {
        final Request request = new Request.Builder()
                .url(URL)
                .post(RequestBody.create(RAW, cmd))
                .build();
        try (
            Response response = client.newCall(request).execute()) {
            return(response.body().string());
        }
    }
}
